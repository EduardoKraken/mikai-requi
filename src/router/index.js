import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import rutas from '@/router'

import Login                       from '@/views/Login.vue'
import Home                        from '@/views/Home.vue'
import Departamentos               from '@/views/Departamentos.vue'
import Requisiciones               from '@/views/Requisiciones.vue'
import MisRequisiciones            from '@/views/MisRequisiciones.vue'
import RequisicionesDepartamento   from '@/views/RequisicionesDepartamento.vue'
import Usuarios                    from '@/views/Usuarios.vue'

// Reportes


Vue.use(VueRouter)

// const routes: 

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    //MODULO DE LOGIN
    { path: '/login/:id'                      , name: 'login'                      , component: Login, 
      meta: { libre: true}},

    { path: '/home'                           , name: 'home'                       , component: Home, 
      meta: { ADMIN: true, USUARIO: true}},

    { path: '/departamentos'                  , name: 'Departamentos'              , component: Departamentos, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/requisiciones'                  , name: 'Requisiciones'              , component: Requisiciones, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/misrequisiciones'               , name: 'MisRequisiciones'           , component: MisRequisiciones, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/usuarios'                       , name: 'Usuarios'                   , component: Usuarios, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/requisicionesdepartamento'      , name: 'RequisicionesDepartamento'  , component: RequisicionesDepartamento, 
      meta: { ADMIN: true, USUARIO: true }},
  ]
})

router.beforeEach( (to, from, next) => {
  // console.log('Entro',store.state.Login.datosUsuario)

  if(to.matched.some(record => record.meta.libre)){
    next()
  }else if(store.state.Login.datosUsuario.admin === 'ADMIN'){
    if(to.matched.some(record => record.meta.ADMIN)){
      next()
    }
  }else if(store.state.Login.datosUsuario.admin === 'USUARIO' || store.state.Login.datosUsuario.admin === 'SUPERVISOR'){
    if(to.matched.some(record => record.meta.USUARIO)){
      next()
    }
  }else{
    next({
      name: 'login'
    })
  }
})

export default router
