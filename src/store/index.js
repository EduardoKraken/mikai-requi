import Vue from 'vue'
import Vuex from 'vuex'

import router   from '@/router'

import Login     from '@/modules/Login/Login'
import  createPersistedState  from  'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	
  },
  mutations: {

  },
  actions: {
  	
  },

  getters:{
		
	},

	modules:{
		Login,
	},
	plugins: [createPersistedState()]

})

